from pathplanner import NavMap, Point, Obstacle, distance, NoPathFoundError
from typing import List, Optional
import random


def create_death_stars(space: NavMap, num_death_stars: int) -> List[Obstacle]:
    """
    Generate a set of non-overlapping death stars
    """
    death_stars: List[Obstacle] = []
    for _ in range(num_death_stars * 10):
        radius = random.randint(1, 5)
        candidate = Obstacle(Point(x=random.randint(radius, space.width-radius-1),
                                   y=random.randint(radius, space.height-radius-1)),
                             radius=radius)
        overlap = False
        for ds in death_stars:
            if (distance(ds.center, candidate.center)
                    <= ds.radius + candidate.radius):
                overlap = True
                break
        if overlap:
            continue
        death_stars.append(candidate)
        if len(death_stars) >= num_death_stars:
            break
    return death_stars


def create_navigable_map(width: int,
                         height: int,
                         bot_radius: int,
                         num_death_stars: int,
                         start: Point,
                         goal: Point) -> NavMap:
    """
    Make a map that has a guaranteed path from start to goal"
    """
    for i in range(100):
        m = NavMap(width, height, bot_radius)
        death_stars = create_death_stars(m, num_death_stars)
        m.add_obstacles(death_stars)
        try:
            print(f"Trying map {i}")
            m.find_path(start, goal)
            return m
        except NoPathFoundError:
            continue
    raise NoPathFoundError


def navigate() -> None:
    width = 100
    height = 100
    bot_radius = 5
    num_death_stars = 40

    start = Point(0, height-1)
    goal = Point(width-1, 0)
    m = create_navigable_map(width=width,
                             height=height,
                             bot_radius=bot_radius,
                             num_death_stars=num_death_stars,
                             start=start,
                             goal=goal)
    path = m.find_path(start, goal)
    print(m.draw(path))


if __name__ == "__main__":
    navigate()
