import dataclasses
from collections import defaultdict
from typing import List, NamedTuple, Iterable, Union, Dict, Tuple
import enum
import heapq
import math
import copy
import itertools
from pathlib import Path
import json


class NoPathFoundError(Exception):
    pass


@dataclasses.dataclass(order=True, frozen=True)
class Point():
    x: int
    y: int


@dataclasses.dataclass
class Obstacle():
    center: Point
    radius: int


class Tile(enum.Enum):
    WALL = "W"
    EMPTY = " "
    BUFFER = "B"

    def __str__(self):
        return self.value


class NavMap(object):
    """
    A navigation map that provides path planning around obstacles.
    """
    def __init__(self, width: int, height: int, bot_radius: int):
        self.width = width
        self.height = height
        self.bot_radius = bot_radius
        self.obstacles: List[Obstacle] = []
        self.map: Dict[Point, Tile] = defaultdict(lambda: Tile.EMPTY)

    def add_obstacles(self, obstacles: Iterable[Obstacle]) -> None:
        """
        Add obstacles to the map
        """
        self.obstacles.extend(obstacles)
        self._update_map()

    def _update_map(self):
        """
        Populate tiles defined by self.obstacles.
        Each obstacle has its own area, plus a buffer around it to account
        for the robot footprint.
        """
        self.map = defaultdict(lambda: Tile.EMPTY)
        for obstacle in self.obstacles:
            center = obstacle.center
            radius = obstacle.radius
            buffer_radius = radius + self.bot_radius
            for row in range(center.y-buffer_radius, center.y+buffer_radius+1):
                for col in range(center.x-buffer_radius, center.x+buffer_radius+1):
                    p = Point(col, row)
                    if self[p] == Tile.WALL:
                        continue
                    dist = distance(center, p)
                    if dist <= radius:
                        self[p] = Tile.WALL
                    elif dist <= buffer_radius:
                        self[p] = Tile.BUFFER

    def __getitem__(self, key: Point) -> Tile:
        return self.map[key]

    def __setitem__(self, key: Point, newvalue: Tile) -> None:
        self.map[key] = newvalue

    def draw(self, path: Iterable[Point]=None) -> str:
        """
        Draw the this map to a string including all obstacles and the path
        if supplied.
        """
        elements = []
        for row in range(self.height):
            for col in range(self.width):
                point = Point(col, row)
                if path and point in path:
                    elements.append("+")
                else:
                    tile = self.map[point]
                    if self.map[point] is Tile.BUFFER:
                        elements.append(str(Tile.EMPTY))
                    else:
                        elements.append(str(tile))
            elements.append('\n')
        return "".join(elements)

    def __contains__(self, key: Point) -> bool:
        return (key.x >= 0
                and key.x < self.width
                and key.y >= 0
                and key.y < self.height)

    def neighbors(self, point: Point) -> List[Point]:
        """
        Return a list of empty neighbor points.  Empty neighbors are points
        within the bounds of this map that are neither within obstacles
        nor within the buffers around obstacles
        """
        points = []
        for x, y in itertools.product((-1, 0, 1), repeat=2):
            if x == y == 0:
                continue
            neighbor = Point(point.x + x, point.y + y)
            if neighbor in self and self[neighbor] not in [Tile.WALL,
                                                           Tile.BUFFER]:
                points.append(neighbor)
        return points

    def to_file(self, filepath: Path) -> None:
        """
        Serialize this map to a file in json format
        """
        out = {"width": self.width,
               "height": self.height,
               "bot_radius": self.bot_radius,
               "obstacles": [dataclasses.asdict(o) for o in self.obstacles]}
        with filepath.open("w") as f:
            json.dump(out, f, indent=4)

    @classmethod
    def from_file(cls, filepath: Path) -> "NavMap":
        """
        Create and return a new NavMap defined in the file at filepath
        """
        with filepath.open("r") as f:
            data = json.load(f)
        new_map = NavMap(width=data["width"],
                         height=data["height"],
                         bot_radius=data["bot_radius"])
        new_map.add_obstacles((Obstacle(Point(x=o["center"]["x"],
                                              y=o["center"]["y"]),
                                        radius=o["radius"])
                               for o in data["obstacles"]))
        return new_map

    def find_path(self, start: Point, goal: Point) -> List[Point]:
        """
        Find a path from start to goal using A*, raise an error if
        no path could be found or return the list of Points leading
        from start to goal
        """

        # The set of points to explore next.  The point with the smallest
        # estimated total path length from start to goal is always
        # on top.
        openSet = MinHeap()
        openSet.push((0, start))

        # For each point, store the previous point in the path
        cameFrom: Dict[Point, Point] = {}

        # The length of the path from start to this point
        gScore: Dict[Point, float] = defaultdict(lambda: math.inf)
        gScore[start] = 0

        # The total estimated length of the path from start to goal
        # if passing through this point.
        fScore: Dict[Point, float] = defaultdict(lambda: math.inf)
        fScore[start] = distance(start, goal)

        while openSet:
            # get the cheapest Point so far
            _, current = openSet.pop()
            if current == goal:
                # If we're done, trace the path we took and return it
                return trace_path(cameFrom, current)
            # self.neighbors() will never return a wall or a Point outside the map
            for neighbor in self.neighbors(current):
                gScore_temp = gScore[current] + distance(current, neighbor)
                if gScore_temp < gScore[neighbor]:
                    # if gScore_temp is lower, it's cheaper to get to neighbor
                    # through current than through any previously explored Point
                    cameFrom[neighbor] = current
                    gScore[neighbor] = gScore_temp
                    fScore[neighbor] = gScore[neighbor] + distance(neighbor, goal)
                    if neighbor not in openSet:
                        # add neighbor to the set of Points to explore next
                        openSet.push((fScore[neighbor], neighbor))
        raise NoPathFoundError()


class MinHeap(object):
    """
    A heap that takes Tuple[float, Point] and keeps the lowest value
    item on top based on the value of the float
    """
    def __init__(self):
        self._heap = []

    def push(self, item: Tuple[float, Point]) -> None:
        heapq.heappush(self._heap, item)

    def pop(self) -> Tuple[float, Point]:
        return heapq.heappop(self._heap)

    def __bool__(self) -> bool:
        return bool(self._heap)

    def __contains__(self, point: Point) -> bool:
        for item in self._heap:
            if item[1] == point:
                return True
        return False


def distance(start: Point, goal: Point) -> float:
    """
    Linear disatnce from start to goal
    """
    return math.sqrt(((start.x - goal.x)**2) + ((start.y - goal.y)**2))


def trace_path(cameFrom: Dict[Point, Point], current: Point) -> List[Point]:
    """
    Reconstruct the path from current through each parent.
    """
    path = [current]
    while current in cameFrom:
        current = cameFrom[current]
        path.insert(0, current)
    return path
