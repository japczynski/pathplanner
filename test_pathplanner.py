import pytest
from pathplanner import (NavMap,
                         Point,
                         Obstacle,
                         distance,
                         MinHeap,
                         Tile,
                         NoPathFoundError)
import copy
import tempfile
from pathlib import Path


def test_generate_map(width=5, height=10, bot_radius=0):
    m = NavMap(width=width, height=height, bot_radius=0)
    assert m.width == width
    assert m.height == height
    assert m.bot_radius == bot_radius


def test_map_to_string():
    m = NavMap(5, 10, 0)
    assert m.draw() == ("     \n"
                        "     \n"
                        "     \n"
                        "     \n"
                        "     \n"
                        "     \n"
                        "     \n"
                        "     \n"
                        "     \n"
                        "     \n")
    m[Point(1, 1)] = Tile.WALL
    assert m.draw() == ("     \n"
                        " W   \n"
                        "     \n"
                        "     \n"
                        "     \n"
                        "     \n"
                        "     \n"
                        "     \n"
                        "     \n"
                        "     \n")

def test_map_obstacle():
    m = NavMap(width=9, height=9, bot_radius=0)
    m.add_obstacles([Obstacle(Point(x=4, y=4), 2)])
    assert m.draw() == ("         \n"
                        "         \n"
                        "    W    \n"
                        "   WWW   \n"
                        "  WWWWW  \n"
                        "   WWW   \n"
                        "    W    \n"
                        "         \n"
                        "         \n")

def test_add_obstacles():
    m = NavMap(width=20, height=20, bot_radius=0)
    m.add_obstacles([Obstacle(Point(5, 5), 2),
                     Obstacle(Point(15, 10), 3)])
    print(m.draw())
    assert m.draw() == (
        "                    \n"
        "                    \n"
        "                    \n"
        "     W              \n"
        "    WWW             \n"
        "   WWWWW            \n"
        "    WWW             \n"
        "     W         W    \n"
        "             WWWWW  \n"
        "             WWWWW  \n"
        "            WWWWWWW \n"
        "             WWWWW  \n"
        "             WWWWW  \n"
        "               W    \n"
        "                    \n"
        "                    \n"
        "                    \n"
        "                    \n"
        "                    \n"
        "                    \n"
    )

def test_distance():
    assert distance(Point(1, 5), Point(-2, 1)) == 5


def test_heap():
    heap = MinHeap()
    heap.push((0, Point(1, 1)))
    heap.push((10, Point(2, 2)))
    heap.push((5, Point(3, 3)))

    assert Point(1, 1) in heap
    assert Point(2, 2) in heap
    assert Point(3, 3) in heap
    assert Point(5, 5) not in heap

    assert heap

    _, point = heap.pop()
    assert point == Point(1, 1)
    _, point = heap.pop()
    assert point == Point(3, 3)
    _, point = heap.pop()
    assert point == Point(2, 2)

    assert not heap

def test_find_path():
    m = NavMap(19, 19, 2)
    m.add_obstacles((Obstacle(Point(10, 10), 5), ))
    path = m.find_path(Point(0, 0), Point(18, 18))
    print(m.draw(path))

def test_no_path():
    m = NavMap(19, 19, 2)
    m.add_obstacles((Obstacle(Point(10, 10), 5), ))
    with pytest.raises(NoPathFoundError):
        m.find_path(Point(0, 0), Point(10, 10))

def test_point():
    assert Point(0, 0) < Point(1, 1)
    assert Point(0, 0) < Point(0, 1)
    assert Point(0, 0) < Point(1, 0)

def test_to_from_file():
    m = NavMap(20, 20, 5)
    m.add_obstacles((Obstacle(Point(6, 6), 2),
                     Obstacle(Point(14, 14), 2)))
    with tempfile.TemporaryDirectory() as d:
        file_path = Path(d) / "map_file"
        m.to_file(file_path)
        m2 = NavMap.from_file(file_path)
    assert m.draw() == m2.draw()
